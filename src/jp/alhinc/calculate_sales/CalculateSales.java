package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales{

	/**
	 * ファイル名・メッセージ定義
	 */
	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";
	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";
	// 商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NAME_BRANCH = "支店定義ファイル";
	private static final String FILE_NAME_COMMODITY = "商品定義ファイル";
	private static final String FILE_NOT_EXIST = "が存在しません";
	private static final String SALES_FILE_NOT_SERIAL = "売上ファイル名が連番になっていません";
	private static final String SALES_AMOUNT_OVER = "合計金額が10桁を超えました";
	private static final String BRANCH_CODE_INVALID = "の支店コードが不正です";
	private static final String COMMODITY_CODE_INVALID = "の商品コードが不正です";
	private static final String FILE_INVALID_FORMAT = "のフォーマットが不正です";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args){
		// コマンドライン引数が渡されているか確認
		if(args.length != 1){
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		// 商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		// 商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, "^[0-9]{3}$", FILE_NAME_BRANCH)){
			return;
		}
		// 商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales, "^[0-9a-zA-Z]{8}$",
				FILE_NAME_COMMODITY)){
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		// 全ファイルの取得
		File[] files = new File(args[0]).listFiles();
		ArrayList<File> rcdFiles = new ArrayList<>();

		// ファイルなのか確認 ＆ 売上ファイル（「8桁」+「.rcd」）の検索
		for(int i = 0; i < files.length; i++){
			if(files[i].isFile()){
				if(files[i].getName().matches("^[0-9]{8}.+rcd$")){
					rcdFiles.add((files[i]));
				}
			}
		}
		// 売上ファイルを昇順並び替え
		Collections.sort(rcdFiles);
		// 売上ファイルが連番になっているか確認
		for(int i = 0; i < rcdFiles.size() - 1; i++){
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));

			if(latter - former != 1){
				System.out.println(SALES_FILE_NOT_SERIAL);
				return;
			}
		}

		for(int i = 0; i < rcdFiles.size(); i++){
			BufferedReader br = null;

			try{
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);

				// 1行ずつ読み込み、リストに追加して保持
				String line;
				List<String> items = new ArrayList<String>();
				while((line = br.readLine()) != null){
					items.add(line);
				}

				// 売上ファイルのフォーマットが正しいか確認
				if(items.size() != 3){
					System.out.println(rcdFiles.get(i).getName() + FILE_INVALID_FORMAT);
					return;
				}

				// 支店コードが存在するか確認
				if(!branchNames.containsKey(items.get(0))){
					System.out.println(rcdFiles.get(i).getName() + BRANCH_CODE_INVALID);
					return;
				}
				// 商品コードが存在するか確認
				if(!commoditySales.containsKey(items.get(1))){
					System.out.println(rcdFiles.get(i).getName() + COMMODITY_CODE_INVALID);
					return;
				}

				// 売上金額が数値かどうか確認
				if(!items.get(2).matches("^[0-9]+$")){
					System.out.println(UNKNOWN_ERROR);
					return;
				}
				// 合計金額が10桁を超えていないか確認
				Long branchSum = branchSales.get(items.get(0)) + Long.parseLong(items.get(2));
				Long commoditySum = commoditySales.get(items.get(1)) + Long.parseLong(items.get(2));
				if(branchSum >= 1000000000L || commoditySum >= 1000000000L){
					System.out.println(SALES_AMOUNT_OVER);
					return;
				}
				// 読み込んだデータをMapに加算
				branchSales.put(items.get(0), branchSum);
				commoditySales.put(items.get(1), commoditySum);
			}catch(IOException e){
				System.out.println(UNKNOWN_ERROR);
				return;
			}finally{
				// ファイルを開いている場合
				if(br != null){
					try{
						// ファイルを閉じる
						br.close();
					}catch(IOException e){
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)){
			return;
		}
		// 商品別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)){
			return;
		}

	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> mapNames,
			Map<String, Long> mapSales, String regex, String fileType){
		BufferedReader br = null;

		try{
			File file = new File(path, fileName);
			// 支店・商品定義ファイルが存在するか確認
			if(!file.exists()){
				System.out.println(fileType + FILE_NOT_EXIST);
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			while((line = br.readLine()) != null){
				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				// カンマ「,」でデータを分割する（items[0]に支店・商品コード、items[1]に支店・商品名を格納）
				String[] items = line.split(",");

				// 定義ファイルのフォーマットが正しいか確認
				if(items.length != 2 || !items[0].matches(regex)){
					System.out.println(fileType + FILE_INVALID_FORMAT);
					return false;
				}

				// 分割したデータを初期値としてMapに追加
				// branchSalesについても初期値として0を追加
				mapNames.put(items[0], items[1]);
				mapSales.put(items[0], 0L);
			}

		}catch(IOException e){
			System.out.println(UNKNOWN_ERROR);
			return false;
		}finally{
			// ファイルを開いている場合
			if(br != null){
				try{
					// ファイルを閉じる
					br.close();
				}catch(IOException e){
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> mapNames,
			Map<String, Long> mapSales){
		// ※ここに書き込み処理を作成してください。(処理内容3-1)

		File file = new File(path, fileName);
		BufferedWriter bw = null;

		try{
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);
			for(String key : mapNames.keySet()){
				bw.write(key + "," + mapNames.get(key) + ",");
				bw.write(mapSales.get(key).toString());
				bw.newLine();
			}

		}catch(IOException e){
			System.out.println(UNKNOWN_ERROR);
			return false;
		}finally{
			if(bw != null){
				try{
					bw.close();
				}catch(IOException e){
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}

		return true;
	}

}
